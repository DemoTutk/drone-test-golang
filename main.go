package main

import (
	"fmt"
)

func HelloWorld() string {
	return "Hello World, drone 999!"
}

func main() {
	fmt.Println(HelloWorld())
}